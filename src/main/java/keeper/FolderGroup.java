package keeper;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@ToString
public class FolderGroup {

    @Getter @Setter
    private String parent;
    @Getter @Setter
    private List<Folder> subfolders;

    public FolderGroup(String parent, List<Folder> subfolders){
        this.parent = parent;
        this.subfolders = new ArrayList<>(subfolders);
    }
}