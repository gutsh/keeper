package keeper.data;

import keeper.MenuItem;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ItemRepository extends MongoRepository<MenuItem, String> {
}
