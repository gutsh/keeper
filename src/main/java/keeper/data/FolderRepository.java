package keeper.data;

import keeper.Folder;
import keeper.FolderGroup;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface FolderRepository extends MongoRepository<Folder, ObjectId> {

    Folder findByName (String name);

    List<Folder> findByLevel (int level);

    @Aggregation("{$group : {_id : null, maxDepth : {$max : $level}}}")
    int maxDepth();

    @Aggregation("{$match : {level : ?0}}, {$group : {_id : $parent, subfolders : {$push : $$ROOT} } }")
    AggregationResults<FolderGroup> findOnLevelAndGroupByParent(int level);
}
