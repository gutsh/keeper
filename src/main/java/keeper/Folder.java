package keeper;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ToString
@Document
public class Folder {

    @MongoId
    private ObjectId id;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private String parent;
    @Indexed
    @Getter @Setter
    private int level;
    @Getter @Setter
    private List<String> subitems;

    public Folder(){
        this.subitems = new ArrayList<>();
    }

    public void addItem(String item){
        this.subitems.add(item);
    }

    public String toJson(List<String> subfolders){
        org.bson.Document document = new org.bson.Document()
                .append("name", this.name)
                .append("subfolders", Arrays.asList(subfolders))
                .append("subitems", this.subitems);
        return document.toJson();
    }
}