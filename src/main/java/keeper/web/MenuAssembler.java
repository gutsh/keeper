package keeper.web;

import keeper.Folder;
import keeper.FolderGroup;
import keeper.data.FolderRepository;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/menu")
public class MenuAssembler {

    private static final Logger LOGGER = LoggerFactory.getLogger(EditController.class);
    FolderRepository folderRepo;

    @Autowired
    public MenuAssembler(FolderRepository folderRepo){
        this.folderRepo = folderRepo;
    }

    @GetMapping(path="/assemble", produces="application/json")
    public String assembleMenu(){

        int maxDepth = folderRepo.maxDepth();
        //List<Folder> _folders;
        List<FolderGroup> _groups;
        //List<String> _parents;

        for(int depth = maxDepth; depth>=0; depth--){

            LOGGER.info("Current depth is "+depth);
            if(depth==maxDepth){
                _groups = new ArrayList<>();
            }
            else{
                AggregationResults<FolderGroup> results = folderRepo.findOnLevelAndGroupByParent(depth+1);
                Document raw = results.getRawResults();
                _groups = new ArrayList<>(results.getMappedResults());
            }

            //_parents = new ArrayList<>();
            for(FolderGroup group : _groups){
                //_parents.add(group.getParent());
                LOGGER.info(group.toString());
            }
            /*
            _folders = new ArrayList<>(folderRepo.findByLevel(depth));
            for(Folder folder : _folders){
                FolderGroup optGroup = new FolderGroup("", new ArrayList<>());
                if(_parents.contains(folder.getName())){
                    optGroup = _groups.get(_parents.indexOf(folder.getName()));
                    LOGGER.info(optGroup.toString());
                }
                LOGGER.info(folder.toString());
                folder.toJson(optGroup.getSubfolders());
                folderRepo.save(folder);
                LOGGER.info(folder.toString());
            }*/
        }
        return "zxc";
        //return folderRepo.findByLevel(0).get(0).toJson();
    }
}
