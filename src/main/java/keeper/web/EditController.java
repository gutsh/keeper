package keeper.web;

import keeper.Folder;
import keeper.MenuItem;
import keeper.data.FolderRepository;
import keeper.data.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.web.bind.annotation.*;

@RepositoryRestController
public class EditController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EditController.class);
    FolderRepository folderRepo;
    ItemRepository itemRepo;

    @Autowired
    public EditController (FolderRepository folderRepo, ItemRepository itemRepo){
        this.folderRepo = folderRepo;
        this.itemRepo = itemRepo;
    }

    @PostMapping(path="/folders", params = {"parent"}, consumes = "application/json")
    @ResponseBody
    Folder injectFolder(@RequestParam("parent") String parent, @RequestBody Folder folder){

        LOGGER.info("Started injecting folder "+folder.getName()+"...");

        if (parent == null){
            folder.setLevel(0);
            folder.setParent(null);
            folderRepo.save(folder);
            LOGGER.info("Folder injected!");
            return folder;
        }

        Folder optParent = folderRepo.findByName(parent);
        folder.setLevel(optParent.getLevel()+1);
        folder.setParent(parent);
        folderRepo.save(folder);
        LOGGER.info("Folder injected!");
        LOGGER.info("Your menu is "+folderRepo.maxDepth()+" levels deep.");
        return folder;
    }

    @PostMapping(path="/items", params = {"parent"}, consumes = "application/json")
    @ResponseBody
    Folder injectItem(@RequestParam("parent") String parent, @RequestBody MenuItem item){

        LOGGER.info("Started injecting item "+item.getName()+"...");

        itemRepo.save(item);
        Folder optParent = folderRepo.findByName(parent);
        optParent.addItem(item.getName());
        folderRepo.save(optParent);
        LOGGER.info("Item injected!");
        return optParent;
    }
}
