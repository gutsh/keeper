package keeper;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;
import org.springframework.data.rest.core.annotation.RestResource;

@Data
@Document
@RestResource(rel="items", path="items")
public class MenuItem {

    @MongoId
    private ObjectId id;
    private String name;
    private int price;
    private String description;
}
