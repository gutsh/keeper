package keeper;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import keeper.web.EditController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(EditController.class)
public class EditControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testMenuAssembling() throws Exception{
        mockMvc.perform(get("/api/menu"))
                .andExpect(status().isOk());
    }
}
